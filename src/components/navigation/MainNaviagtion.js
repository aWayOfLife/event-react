import {NavLink} from 'react-router-dom'
import React from 'react';
import  './MainNavigation.css'
import AuthContext from '../../context/auth-context'
import {useContext} from 'react'

function MainNaviagtion() {
    const authContext = useContext(AuthContext);
    return (
        <header className="main-navigation">
            <div className="main-navigation-logo">
                <h1>EasyEvent</h1>
            </div>
            <nav className="main-navigation-items">
                <ul>
                    <li><NavLink to="/events">Events</NavLink></li>
                    {authContext.token && <li><NavLink to="/bookings">Bookings</NavLink></li>}
                    {!authContext.token && <li><NavLink to="/auth">Authentication</NavLink></li>}
                    {authContext.token && <li><button onClick={authContext.logout}>Logout</button></li>}

                </ul>
            </nav>
        </header>
    )
}

export default MainNaviagtion
