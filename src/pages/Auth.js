import React from 'react'
import './Auth.css'
import {useState, useContext} from 'react'
import AuthContext from '../context/auth-context'

function Auth() {
    const context = useContext(AuthContext)
    const [isLogin,setIsLogin] = useState(true)
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')
    const submitHandler = (e) =>{
        e.preventDefault()
        console.log(email,password)
        let requestBody = {
            query: `
                query {
                    login(email:"${email}", password:"${password}"){
                        userId
                        token
                        tokenExpiration
                    }
                }
            `
        }

        if(!isLogin){
            requestBody = {
            query: `
                mutation {
                    createUser(userInput:{email:"${email}", password:"${password}"}){
                        _id
                        email
                    }
                }
            `
        }
        }
        

        fetch('http://localhost:5000/graphql',{
            method:'POST',
            body:JSON.stringify(requestBody),
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(res =>{
            if(res.status!==200 && res.status!==201){
                throw new Error("Failed")
            }
            return res.json()
        }).then(res=>{
            if(isLogin){
                context.login(res.data.login.token, res.data.login.userId, res.data.login.tokenExpiration)
            }
            console.log(res)
        }).catch(err =>{
            console.log(err)
        })
    }
    return (
        <form className="auth-form" onSubmit={submitHandler}>
            <div className="form-control">
                <label htmlFor="email">Email</label>
                <input type="email" value={email} onChange={(e)=> setEmail(e.target.value)} id="email"/>
            </div>
            <div className="form-control">
                <label htmlFor="password">Password</label>
                <input type="password" value={password} onChange={(e)=> setPassword(e.target.value)} id="password"/>
            </div>
            <div className="form-actions">
                <button type="button" onClick={()=>setIsLogin(!isLogin)}>{isLogin?'Switch to Signup':'Switch to Login'}</button>
                <button type="submit">Submit</button>
            </div>
        </form>
    )
}

export default Auth
