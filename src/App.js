import './App.css';
import {BrowserRouter, Route, Redirect, Switch} from 'react-router-dom'
import Auth from './pages/Auth';
import Events from './pages/Events';
import Bookings from './pages/Bookings';
import MainNaviagtion from './components/navigation/MainNaviagtion';
import AuthContext from './context/auth-context'
import {useState} from 'react'

function App() {
  const [state, setState] = useState({
    token:null,
    userId:null
  })
  const login = (token,userId, tokenExpiration) =>{
    setState({token:token, userId:userId})
  }
  const logout = () =>{
    setState({token:null, userId:null})

  }
  return (
    <BrowserRouter>
      <>
      <AuthContext.Provider value={{token:state.token, userId:state.userId, login: login, logout:logout}}>
        <MainNaviagtion/>
        <main className="main-content">
          <Switch>
            {!state.token && < Redirect path="/" to="/auth" exact/>}
            {!state.token && < Redirect path="/bookings" to="/auth" exact/>}
            {state.token && < Redirect path="/" to="/events" exact/>}
            {state.token && < Redirect path="/auth" to="/events" exact/>}
            {!state.token && <Route path="/auth" component={Auth}/>}
            <Route path="/events" component={Events}/>
            {state.token && <Route path="/bookings" component={Bookings}/>}
          </Switch>
        </main>
        </AuthContext.Provider>
      </>
    </BrowserRouter>
  );
}

export default App;
